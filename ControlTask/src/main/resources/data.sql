insert into edition(title, discription, count_books, state) values ('Эксмо — АСТ ', 'это самая крупная издательская группа в России, занимающая более 20% российского книжного рынка после слияния.', '40', 'AVAILABLE');
insert into edition(title, discription, count_books, state) values ('ОЛМА Медиа Групп', 'российское издательство, занимающее  более 14% российского книжного рынка. ', '28', 'AVAILABLE');
insert into edition(title, discription, count_books, state) values ('Азбука — Аттикус', 'одна из крупнейших книгоиздательских групп в России, основанная в 2008 году. ', '15', 'AVAILABLE');
insert into edition(title, discription, count_books, state) values ('РИПОЛ', 'многопрофильное издательство, один из лидеров российского книжного рынка.', '35', 'AVAILABLE');
insert into edition(title, discription, count_books, state) values ('Экзамен', 'книжное издательство, приоритетным направлением которого являются материалы для подготовки учащихся к ОГЭ и ЕГЭ.', '17', 'AVAILABLE');
insert into edition(title, discription, count_books, state) values ('Росмэн', 'етское издательство №1 в России, занимающее 2% российского книжного рынка. ', '19', 'AVAILABLE');
insert into edition(title, discription, count_books, state) values ('Издательский дом «Лев', 'крупная российская компания,один из крупнейших игроков на рынке детской литературы.', '0', 'NOAVAILABLE');
insert into edition(title, discription, count_books, state) values ('Феникс', 'крупнейшее региональное издательство России, расположенное в Ростове-на-Дону и выпускающее учебные пособия для ВУЗов и школ.', '0', 'NOAVAILABLE');
insert into edition(title, discription, count_books, state) values ('Фламинго', 'издательство, специализирующееся на издании книг для дошкольников и учеников начальных классов', '0', 'ORDER');
insert into edition(title, discription, count_books, state) values ('Ювента', 'издательство, специализирующееся на учебной и методической литературе для дошкольников,а также для начальной и основной школы.', '0', 'ORDER');

