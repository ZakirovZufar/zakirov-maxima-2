package ru.maxima.blog.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.blog.dto.EditionDto;
import ru.maxima.blog.dto.EditionPage;
import ru.maxima.blog.exceptions.EditionNotFoundException;
import ru.maxima.blog.models.Edition;
import ru.maxima.blog.repositories.EditionRepository;
import ru.maxima.blog.services.EditionService;

import static ru.maxima.blog.dto.EditionDto.from;

/**
 * Created by Зульфат on 23.05.2022.
 */
@Service
@RequiredArgsConstructor
public class EditionServiceImpl implements EditionService {

    private final int DEFAULT_SIZE_PAGE = 5;

    private final EditionRepository editionRepository;

    @Transactional
    @Override
    public EditionDto addEdition(EditionDto edition) {
        Edition newEdition = Edition.builder()
                .title(edition.getTitle())
                .discription(edition.getDiscription())
                .countBooks(edition.getCountBooks())
                .state(Edition.State.NOAVAILABLE)
                .build();
        editionRepository.save(newEdition);
        return from(newEdition);
    }

    @Transactional
    @Override
    public EditionPage getAll(int page) {
        PageRequest pageRequest = PageRequest.of(page,DEFAULT_SIZE_PAGE, Sort.by("id"));
        Page<Edition> editions = editionRepository.findAllByState(Edition.State.AVAILABLE, pageRequest);

        return EditionPage.builder()
                .editions(from(editions.getContent()))
                .pageSize(editions.getSize())
                .totalPages(editions.getTotalPages())
                .build();
    }

    @Transactional
    @Override
    public EditionDto getEdition(Long editionId) {
        return from(getEditionOrThrow(editionId));
    }

    private Edition getEditionOrThrow(Long editionId) {
        return editionRepository.findById(editionId).orElseThrow(EditionNotFoundException::new);
    }


}
