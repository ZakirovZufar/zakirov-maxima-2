package ru.maxima.blog.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.maxima.blog.dto.SignUpDto;
import ru.maxima.blog.dto.UserDto;
import ru.maxima.blog.models.User;
import ru.maxima.blog.repositories.UserRepository;
import ru.maxima.blog.services.SignUpService;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.Set;

import static ru.maxima.blog.dto.UserDto.from;

/**
 * Created by Зульфат on 29.05.2022.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final Validator validator;
    private final UserRepository userRepository;

    @Override
    public UserDto signUp(SignUpDto date) {
        User user = User.builder()
                .firstName(date.getFirstName())
                .lastName(date.getLastName())
                .email(date.getEmail())
                .hashPassword(date.getPassword())
                .role(User.Role.USER)
                .state(User.State.CONFIRMED)
                .build();

        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if (violations.isEmpty()) {
            userRepository.save(user);
            return from(user);
        } else {
            throw new ValidationException(processViolations(violations));
        }

    }

    private String processViolations(Set<ConstraintViolation<User>> violations) {
        StringBuilder message = new StringBuilder();

        message.append("Violations[");
        for (ConstraintViolation<User> violation : violations) {
            message.append("field(")
                    .append(
                            violation.getPropertyPath())
                    .append(") - ")
                    .append(violation.getMessage())
                    .append(", ");
        }

        return message.append("]").toString();
    }

}
