package ru.maxima.blog.services;

import ru.maxima.blog.dto.EditionDto;
import ru.maxima.blog.dto.EditionPage;
import ru.maxima.blog.models.Edition;

/**
 * Created by Зульфат on 23.05.2022.
 */

public interface EditionService {

    EditionDto addEdition(EditionDto edition);

    EditionPage getAll(int page);

    EditionDto getEdition(Long editionId);

}
