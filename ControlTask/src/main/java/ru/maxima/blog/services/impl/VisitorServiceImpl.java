package ru.maxima.blog.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.blog.dto.AddVisitorToBookDto;
import ru.maxima.blog.dto.VisitorDto;
import ru.maxima.blog.dto.VisitorPage;
import ru.maxima.blog.exceptions.EditionNotFoundException;
import ru.maxima.blog.exceptions.VisitorNotFoundException;
import ru.maxima.blog.models.Edition;
import ru.maxima.blog.models.Visitor;
import ru.maxima.blog.repositories.EditionRepository;
import ru.maxima.blog.repositories.VisitorRepositories;
import ru.maxima.blog.services.VisitorService;

import static ru.maxima.blog.dto.VisitorDto.from;

/**
 * Created by Зульфат on 26.05.2022.
 */
@Service
@RequiredArgsConstructor
public class VisitorServiceImpl implements VisitorService {

    private final int DEFAULT_SIZE_PAGE = 5;

    private final VisitorRepositories visitorRepository;

    private final EditionRepository editionRepository;

    @Transactional
    @Override
    public VisitorDto addVisitor(VisitorDto visitor) {
        Visitor newVisitor = Visitor.builder()
                .id(visitor.getId())
                .build();
        return from(visitorRepository.save(newVisitor));
    }

    @Transactional
    @Override
    public VisitorDto addVisitorToBook(Long visitorId, AddVisitorToBookDto editionId) {

            Visitor visitor = visitorRepository.findById(visitorId).orElseThrow(VisitorNotFoundException::new);
            Edition edition = editionRepository.findById(editionId.getEditionId()).orElseThrow(EditionNotFoundException::new);

            if (edition.getCountBooks() != 0) {
                visitor.getEditions().add(edition);
                visitorRepository.save(visitor);
            }
            return from(visitor);
        }



    @Transactional
    @Override
    public VisitorDto returnVisitorToBook(Long visitorId, AddVisitorToBookDto editionId) {

        Visitor visitor = visitorRepository.findById(visitorId).orElseThrow(VisitorNotFoundException::new);
        Edition edition = editionRepository.findById(editionId.getEditionId()).orElseThrow(EditionNotFoundException::new);

        if (visitor.getEditions().contains(edition)){
            visitor.getEditions().remove(edition);
            visitorRepository.save(visitor);

        }
        return from(visitor);
    }

    @Transactional
    @Override
    public VisitorPage getAllVisitors(int page) {

        PageRequest pageRequest = PageRequest.of(page, DEFAULT_SIZE_PAGE, Sort.by("id"));
        Page<Visitor> visitors = visitorRepository.findAll(pageRequest);

        return VisitorPage.builder()
                .visitors(from(visitors.getContent()))
                .pageSize(visitors.getSize())
                .totalPages(visitors.getTotalPages())
                .build();
    }
}
