package ru.maxima.blog.services;

import ru.maxima.blog.dto.AddVisitorToBookDto;
import ru.maxima.blog.dto.VisitorDto;
import ru.maxima.blog.dto.VisitorPage;

/**
 * Created by Зульфат on 26.05.2022.
 */
public interface VisitorService {

    VisitorDto addVisitor(VisitorDto visitor);

    VisitorDto addVisitorToBook(Long visitorId, AddVisitorToBookDto editionId);

    VisitorDto returnVisitorToBook(Long visitorId, AddVisitorToBookDto editionId);

    VisitorPage getAllVisitors(int page);

}
