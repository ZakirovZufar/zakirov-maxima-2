package ru.maxima.blog.services;

import ru.maxima.blog.dto.SignUpDto;
import ru.maxima.blog.dto.UserDto;

/**
 * Created by Зульфат on 29.05.2022.
 */
public interface SignUpService {
    UserDto signUp(SignUpDto date);
}
