package ru.maxima.blog.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.blog.models.Edition;

/**
 * Created by Зульфат on 23.05.2022.
 */
public interface EditionRepository extends JpaRepository<Edition, Long> {
    Page<Edition> findAllByState(Edition.State state, Pageable pageable);
}
