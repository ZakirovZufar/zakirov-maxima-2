package ru.maxima.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.blog.models.Visitor;

/**
 * Created by Зульфат on 26.05.2022.
 */
public interface VisitorRepositories extends JpaRepository<Visitor, Long> {
}
