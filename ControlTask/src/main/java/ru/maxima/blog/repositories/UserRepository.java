package ru.maxima.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.blog.models.User;

/**
 * Created by Зульфат on 29.05.2022.
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
