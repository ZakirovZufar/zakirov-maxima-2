package ru.maxima.blog.models;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Зульфат on 23.05.2022.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "editions")
public class Visitor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany
    @JoinTable(name = "visitor_book_edition",
        joinColumns =@JoinColumn(name = "visitor_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "book_edition", referencedColumnName = "id"))
    private Set<Edition> editions;

}
