package ru.maxima.blog.models;

import lombok.*;
import ru.maxima.blog.dto.EditionDto;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Зульфат on 23.05.2022.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "visitors")
public class Edition{

    public enum State{
        AVAILABLE, NOAVAILABLE, ORDER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String discription;
    private Integer countBooks;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @ManyToMany(mappedBy = "editions")
    private Set<Visitor> visitors;
}
