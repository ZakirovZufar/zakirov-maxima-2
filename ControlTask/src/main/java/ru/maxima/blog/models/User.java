package ru.maxima.blog.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Created by Зульфат on 29.05.2022.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "account")
public class User{

    public enum State{
        NOT_CONFIRMED, CONFIRMED, BANNED
    }

    public enum Role{
        ADMIN, USER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;

    @NotBlank(message = "{email.notBlank}")
    @Column(unique = true)
    private String email;

    @NotBlank(message = "{passwordHash.notBlank}")
    private String hashPassword;

    @Enumerated(EnumType.STRING)
    private State state;

    @Enumerated(EnumType.STRING)
    private Role role;


}
