package ru.maxima.blog.exceptions;

/**
 * Created by Зульфат on 29.05.2022.
 */
public class ValidationException extends RuntimeException {
    public ValidationException(String message){
        super(message);
    }
}
