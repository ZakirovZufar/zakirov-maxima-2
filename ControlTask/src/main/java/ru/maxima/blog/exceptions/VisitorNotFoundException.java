package ru.maxima.blog.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Зульфат on 26.05.2022.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class VisitorNotFoundException extends RuntimeException {
}
