package ru.maxima.blog.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.blog.models.Visitor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Зульфат on 26.05.2022.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "Страница с посетителями")
public class VisitorDto {

    @Schema(description = "Идентификатор посетителя")
    private Long id;

    public static VisitorDto from(Visitor visitor){
        return VisitorDto.builder()
                .id(visitor.getId())
                .build();
    }

    public static List<VisitorDto> from(List<Visitor> visitors){
        return visitors.stream().map(VisitorDto::from).collect(Collectors.toList());
    }
}
