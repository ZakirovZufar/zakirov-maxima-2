package ru.maxima.blog.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by Зульфат on 28.05.2022.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "Страница со списком всех посетителей и общее количество таких страниц")
public class VisitorPage {

    @Schema(description = "Список посетителей")
    private List<VisitorDto> visitors;
    @Schema(description = "Размер текущей страницы")
    private Integer pageSize;
    @Schema(description = "Общее количество страниц")
    private Integer totalPages;
}
