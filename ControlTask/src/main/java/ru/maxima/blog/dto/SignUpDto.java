package ru.maxima.blog.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Created by Зульфат on 29.05.2022.
 */
@Schema(description = "Данные о пользователе")
@Data
public class SignUpDto {

    @Schema(description = "Имя", example = "Зуфар")
    private String firstName;

    @Schema(description = "Фамилия", example = "Закиров")
    private String lastName;

    @Schema(description = "email", example = "zakirov.zufar@list.ru")
    @NotBlank(message = "{email.notBlank}")
    private String email;

    @Schema(description = "Пароль")
    @NotBlank(message = "{dto.password.notBlank}")
    private String password;


}
