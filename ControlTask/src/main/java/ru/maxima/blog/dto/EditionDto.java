package ru.maxima.blog.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.blog.models.Edition;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Зульфат on 23.05.2022.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "Издание")
public class EditionDto {

    @Schema(description = "Идентификатор издания")
    private Long id;
    @Schema(description = "Название издания")
    private String title;
    @Schema(description = "Описание издания")
    private String discription;
    @Schema(description = "Количество книг издания")
    private Integer countBooks;

    public static EditionDto from(Edition edition){
        return EditionDto.builder()
                .id(edition.getId())
                .title(edition.getTitle())
                .discription(edition.getDiscription())
                .countBooks(edition.getCountBooks())
                .build();
    }

    public static List<EditionDto> from(List<Edition> editions){
        return editions.stream().map(EditionDto::from).collect(Collectors.toList());
    }
}
