package ru.maxima.blog.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.blog.models.User;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Зульфат on 29.05.2022.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Данные о пользователе")
public class UserDto {

    @Schema(description = "Идентификатор пользователя" ,example = "1")
    private Long id;
    @Schema(description = "Имя" ,example = "Зуфар")
    private String firstName;
    @Schema(description = "Фамилия" ,example = "Закиров")
    private String lastName;

    public static UserDto from(User user){
        return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }

    public static List<UserDto> from(List<User> users){
        return users.stream().map(UserDto::from).collect(Collectors.toList());
    }
}
