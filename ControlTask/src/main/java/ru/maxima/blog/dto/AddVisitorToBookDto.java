package ru.maxima.blog.dto;

import lombok.Data;

/**
 * Created by Зульфат on 26.05.2022.
 */
@Data
public class AddVisitorToBookDto {

    private Long editionId;
}
