package ru.maxima.blog.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import ru.maxima.blog.dto.SignUpDto;
import ru.maxima.blog.dto.UserDto;
import ru.maxima.blog.services.SignUpService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Зульфат on 29.05.2022.
 */
@RequiredArgsConstructor
@RestController
public class SignUpController {

    private final SignUpService signUpService;

    @Operation(summary = "Регистрация пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Информация о созданном пользователе", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))
            })
    })
    @PostMapping("/users")
    public ResponseEntity<UserDto> signUp(@RequestBody @Valid SignUpDto data){
        return ResponseEntity.status(201)
                .body(signUpService.signUp(data));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationException(MethodArgumentNotValidException exception) {
        Map<String, String> response = new HashMap<>();
        exception
                .getBindingResult()
                .getAllErrors()
                .forEach(error ->
                        response.put(((FieldError)error).getField(), error.getDefaultMessage()));

        return response;
    }

}
