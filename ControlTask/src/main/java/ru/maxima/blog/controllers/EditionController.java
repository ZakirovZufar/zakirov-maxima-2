package ru.maxima.blog.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.maxima.blog.dto.EditionDto;
import ru.maxima.blog.dto.EditionPage;
import ru.maxima.blog.services.EditionService;

/**
 * Created by Зульфат on 24.05.2022.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/editions")
public class EditionController {

    private final EditionService editionService;

    @Operation(summary = "Добавление издания")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Информация о добавленном издании", content = {
                    @Content(mediaType = "application/json",
                    schema = @Schema(implementation = EditionDto.class))
            })
    })
    @PostMapping
    public ResponseEntity<EditionDto> addEdition(@RequestBody EditionDto edition){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(editionService.addEdition(edition));
    }

    @Operation(summary = "Получение всех изданий с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница с изданиями", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = EditionPage.class))
            })
    })
    @GetMapping
    public ResponseEntity<EditionPage> getAllEdition(@Parameter(description = "Номер страницы")@RequestParam("page") int page){
        return ResponseEntity
                .ok(editionService.getAll(page));
    }

    @Operation(summary = "Получение издания по идентификатору")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Информация об издании", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = EditionDto.class))
            }),
            @ApiResponse(
                    responseCode = "404", description = "Издание не найдено")
    })
    @GetMapping("/{edition-id}")
    public ResponseEntity<EditionDto> getEdition(@Parameter(description = "Идентификатор издания")@PathVariable("edition-id") Long editionId){
        return ResponseEntity
                .ok(editionService.getEdition(editionId));
    }
}
