package ru.maxima.blog.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.maxima.blog.dto.AddVisitorToBookDto;
import ru.maxima.blog.dto.EditionPage;
import ru.maxima.blog.dto.VisitorDto;
import ru.maxima.blog.dto.VisitorPage;
import ru.maxima.blog.services.VisitorService;

/**
 * Created by Зульфат on 27.05.2022.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/visitors")
public class VisitorController {

    private final VisitorService visitorService;

    @Operation(summary = "Добавление посетителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Информация о добавленном посетители", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = VisitorDto.class))
            })
    })
    @PostMapping
    public ResponseEntity <VisitorDto> addVisitor(@Parameter(description = "Идентификатор посетителя генерируется автоматически")
                                                      @RequestBody VisitorDto visitor){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(visitorService.addVisitor(visitor));
    }

    @Operation(summary = "Взять книгу")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Процесс взятия книги посетителем", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = VisitorDto.class))
            })
    })
    @PostMapping("/{visitor-id}/editions")
    public ResponseEntity<VisitorDto> addVisitorToBook(@PathVariable("visitor-id") Long visitorId ,
                                                       @RequestBody AddVisitorToBookDto editionId){
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(visitorService.addVisitorToBook(visitorId,editionId));
    }

    @Operation(summary = "Посетитель возвращает книгу")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Возврат книги посетителем", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = VisitorDto.class))
            })
    })
    @PostMapping("/{visitor-id}")
    public ResponseEntity<VisitorDto> returnVisitorToBook(@PathVariable("visitor-id") Long visitorId,
                                                               @RequestBody AddVisitorToBookDto editionId) {
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(visitorService.returnVisitorToBook(visitorId, editionId));
    }

    @Operation(summary = "Получение всех посетителей с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница с посетителями", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = VisitorPage.class))
            })
    })
    @GetMapping
    public ResponseEntity<VisitorPage> getAllVisitors(@RequestParam("page") int page){
        return ResponseEntity
                .ok(visitorService.getAllVisitors(page));
    }
}
