
public class LinkedList<E> implements List<E> {

    private static class Node<V> {
        V value;
        Node<V> next;

        Node(V value) {
            this.value = value;
        }
    }

    // ссылка на первый элемент
    private Node<E> first;
    // ссылка на последний элемент
    private Node<E> last;

    private int size = 0;

    public void add(E element) {
        // для нового элемента создаем узел
        Node<E> newNode = new Node<>(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
        } else {
            // следующий после последнего - это новый узел
            last.next = newNode;
        }
        // теперь новый узел - последний
        last = newNode;

        size++;
    }

    public void addToBegin(E element) {
        // для нового элемента создаем узел
        Node<E> newNode = new Node<>(element);

        if (isEmpty()) {
            last = newNode;
        } else {
            // для нового узла, следующий после него - это первый узел списка
            newNode.next = first;
        }

        // теперь новый узел - первый
        first = newNode;
        size++;
    }

    @Override
    public int size() {
        return size;

    }

    public E get(int index) {
        // начинаем с первого элемента
        Node<E> current = first;

        int i = 0;

        while (i < size) {
            // если нашли нужный индекс
            if (i == index) {
                // возвращаем значение текущего узла
                return current.value;
            }
            // иначе идем дальше
            current = current.next;
            i++;
        }

        System.err.println("В списке нет такого индекса");
        return null;
    }

    public void removeAt(int index) {
        Node<E> previousRemove = first;

        if (index == 0) {
            // если удаляется первый элемент, просто сдвигаем ссылку на него
            first = first.next;
            size--;
            return;
        }

        // доходим до индекса элемента, который стоит перед удаляемым
        for (int i = 0; i < index - 1; i++) {
            // переходим по узлам
            previousRemove = previousRemove.next;
        }
        // если удаляем последний
        if (index == (size - 1)) {
            last = previousRemove;
        }
        // в current находится узел, который стоит перед удаляемым
        Node forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void remove(E element) {
        Node<E> firstRemove = first;

        int index = 0;

        if (firstRemove.value == element) {
            removeAt(index);
        }
        for (int i = 0; i < size - 1; i++) {
            index++;
            if (firstRemove.next.value == element) {
                removeAt(index);
                break;
            } else {
                firstRemove = firstRemove.next;
            }
        }

    }

    public void removeAll(E element) {
        Node<E> givenRemove = first;
        for (int i = 0; i < size; i++) {
            if (givenRemove.value == element) {
                remove(element);
            } else {
                givenRemove = givenRemove.next;
            }
        }
    }
}

