
public class ArrayList<T> {
    private final static int DEFAULT_SIZE = 10;

    // хранилище элементов
    private T[] elements;
    // количество фактических элементов в списке
    private int size;

    public ArrayList() {
        // при создании списка я создал внутри массив на 10 элементов
        this.elements = (T[]) new Object[DEFAULT_SIZE];
    }

    /**
     * Добавление элемента в конец списка
     *
     * @param element добавляемый элемент
     */
    public void add(T element) {
        // если у меня переполнен массив
        if (isFullArray()) {
            resize();
        }

        this.elements[size++] = element;
    }

    private void resize() {
        // создаем новый массив, который в полтора раза больше, чем предыдущий
        T[] newArray = (T[]) new Object[this.elements.length + elements.length / 2];
        // копируем элементы из старого массива в новый поэлементно
        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }
        // заменяем ссылку на старый массив ссылкой на новый массив
        // старый массив будет удален java-машиной
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    /**
     * Получение элемента по индексу
     *
     * @param index индекс элемента
     * @return элемент, который был добавлен в список под номером index, если такого индекса нет - ошибка
     */
    public  T get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            System.err.println("В списке нет такого индекса");
           return null;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Удаляет все элементы из списка
     */
    public void clear() {
        size = 0;
    }

    /**
     * Возвращает количество элементов списка
     *
     * @return размер списка
     */
    public int size() {
        return size;
    }

    /**
     * Удаляет элемент в заданном индексе, смещая элементы, которые идут после удаляемого на одну позицию влево
     * <p>
     * 0 -> [14] 1-> [71] 2-> [82] 3-> [25], size = 4
     * <p>
     * removeAt(1)
     * <p>
     * <p>
     * [14] [82] [25] | [25], size = 3
     *
     * @param index индекс удаляемого элемента
     */
    public void removeAt(int index) {
        if (isCorrectIndex(index)) {
            for (int i = index; i < elements.length - 1; i++)
                elements[i] = elements[i + 1];
        } else {
            System.out.println("такого элемента с индексом нет");
        }
        size--;
    }

    /**
     * Удаляет первое вхождение элемента в список
     * <p>
     * 34, 56, 78, 56, 92, 11
     * <p>
     * remove(56)
     * <p>
     * 34, 78, 56, 92, 11
     *
     * @param element
     */
    public void remove(T element) {
        for (int i = 0; i < elements.length - 1; i++) {
            if (elements[i] == element) {
                elements[i] = elements[i + 1];
                break;
            } else {
                System.out.println("Такого элемента нет");
            }

        }
        size--;
    }


    /**
     * Удаляет последнее вхождение элемента в список
     * <p>
     * 34, 56, 78, 56, 92, 11
     * <p>
     * removeLast(56)
     * <p>
     * 34, 56, 78, 92, 11
     *
     * @param element
     */
    public void removeLast(T element) {
        for (int i = elements.length - 1; i >= 0; i--) {
            if (elements[i] == element) {
                elements[i] = elements[i + 1];
                break;
            } else {
                System.out.println("такого элемента нет");
            }
        }
    }

    public void removeAll(T element) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == element) {
                elements[i] = elements[i + 1];
            }
        }
        size--;
    }

    /**
     * Вставляет элемент в заданный индекс (проверяет условие, index < size)
     * Элемент, который стоял под индексом index сдвигается вправо (как и все остальные элементы)
     * 34, 56, 78, 56, 92, 11
     * <p>
     * add(2, 100)
     * <p>
     * 34, 56, 100, 78, 56, 92, 11
     *
     * @param index   куда вставляем элемент
     * @param element элемент, который будем вставлять
     */
    public void add(int index, T element) {

        if (isFullArray()) {
            resize();
        }
        if (isCorrectIndex(index)) {
            for (int i = elements.length - 1; i >= index; i--) {
                elements[i] = elements[i - 1];
            }
            elements[index] = element;
        } else {
            System.out.println("индекс больше размера списка");
        }
        size++;
    }
}

