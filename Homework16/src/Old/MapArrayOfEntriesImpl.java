package Old;//package ru.maxima.Old;
//
//public class MapArrayOfEntriesImpl<K, V> implements Map<K, V> {
//
//    private static final int MAX_MAP_SIZE = 10;
//
//    private Entry<K,V>[] entries;
//
//    private int count;
//
//    public MapArrayOfEntriesImpl() {
//        this.entries = new Entry[MAX_MAP_SIZE];
//    }
//
//    @Override
//    public void put(K key, V value) {
//        // должны проверить, а нет ли уже такого ключа
//        // если есть - надо заменить у него значение
//        for (int i = 0; i < count; i++) {
//            // нашли совпадение
//            if (entries[i].key.equals(key)) {
//                // заменили значение
//                entries[i].value = value;
//                // остановили выполнение метода
//                return;
//            }
//        }
//        this.entries[count] = new Entry<>(key, value);
//        count++;
//    }
//
//    @Override
//    public V get(K key) {
//        // хотим по ключу получить значение, например get("Виктор")
//        // пробегаем все ключи
//        for (int i = 0; i < count; i++) {
//            // если на i-ой позиции мы встретили ключ, который совпал с тем, который подали на вход
//            if (entries[i].key.equals(key)) {
//                // возвращаем соответствующее значение
//                return entries[i].value;
//            }
//        }
//        return null;
//    }
//
//    private static class Entry<K, V> {
//        K key;
//        V value;
//
//        Entry(K key, V value) {
//            this.key = key;
//            this.value = value;
//        }
//    }
//
//}
