package examples;

import collections.Map;
import collections.MapHashImpl;
import collections.Set;



public class MainMap {

    public static void main(String[] args) {
        Map<String, Integer> map = new MapHashImpl<>();

        map.put("Марсель", 27); // +
        map.put("Марсель", 39);
        map.put("Виктор", 25);
        map.put("Айрат", 23); // +
        map.put("Максим", 24);
        map.put("Разиль", 25);
        map.put("Настя", 18); // +
        map.put("Алия", 18); // +
        map.put("Ринат", 28);
        map.put("Рустем", 29);
        map.put("Ирик", 18); // +
        map.put("Равиль", 31); // +
        map.put("Мария", 18); // +
        map.put("Амир", 33); // +
        map.put("Лилия", 18); // +
        map.put("Денис", 33); // +
        map.put("Ильдар", 34); // +
        map.put("Леонид", 35); // +
        map.put("Олег", 36); // +
        map.put("Валентин", 36); // +
        map.put("Никита", 37); // +
        map.put("Вячеслав", 38); // +

//        System.out.println(map.get("Марсель"));
//        System.out.println(map.get("Виктор"));
//        System.out.println(map.get("Айрат"));
//        System.out.println(map.get("Александр"));
//        System.out.println(map.get("Ирик"));
//        System.out.println(map.get("Разиль"));

        System.out.println(map.containsKey("Разиль"));
        System.out.println(map.containsKey("Леонид"));
        System.out.println(map.containsKey("JAVA"));

        Set<String> names = map.keySet();
        Set<Map.MapEntry<String, Integer>> entrySet = map.entrySet();

        int i = 0;
    }

}
