package examples;

import collections.Set;
import collections.SetHashImpl;



public class MainSet {

    public static void main(String[] args) {
        Set<String> set = new SetHashImpl<>();
        set.add("Привет!");
        set.add("Как дела?");
        set.add("Привет!");
        set.add("Как дела?");

        System.out.println(set.contains("Привет"));
        System.out.println(set.contains("Как дела?"));
        System.out.println(set.contains("Марсель"));
    }

}
