import collections.Map;
import collections.MapHashImpl;


import java.util.Arrays;
import java.util.Scanner;

public class mainHomework {

    public static void main(String[] args) {
        Map<String, Integer> count = new MapHashImpl<>();

        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        String words[] = text.split(" ");

        System.out.println(Arrays.toString(words));


        for (int i = 0; i < words.length; i++) {
            if (count.containsKey(words[i])) {
                int lastCount = count.get(words[i]);
                lastCount++;
                count.put(words[i], lastCount);
                System.out.println(words[i] + " " + lastCount);
            }
            else {
                count.put(words[i], 1);
            }

        }
    }
}


