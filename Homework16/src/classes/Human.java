package classes;


public class Human {
    private int id;
    private String firstName;
    private String lastName;

    private int friendsCount;

    public Human(int id, String firstName, String lastName, int friendsCount) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.friendsCount = friendsCount;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getFriendsCount() {
        return friendsCount;
    }
    // equals - метод, который позволяет сравнивать два объекта по нужному нам правилу
    // даже если ссылки на объекты разные, то их можно сравнить по полям
    @Override
    public boolean equals(Object obj) {
        // если одна и та же ссылка
        if (obj == this) {
            // это один и тот же объект
            return true;
        }
        // если это другой объект, но при этом экземпляр Human
        if (obj instanceof Human) {
            // делаем явное преобразование
            Human that = (Human) obj;
            // сравниваем все поля - id, имя, фамилию
            return this.id == that.id
                    && this.firstName.equals(that.firstName)
                    && this.lastName.equals(that.lastName);
        }
        // если мы сравнивали не с Human, то возвращаем false
        return false;
    }

    @Override
    public int hashCode() {
        int hash1 = id;
        int hash2 = firstName.hashCode();
        int hash3 = lastName.hashCode();
//        int hash4 = friendsCount;

        return hash1 + hash2 * 31 + hash3 * 31 * 31;
    }

}
