package classes;

import collections.Map;
import collections.MapHashImpl;



public class Main {

    /**
     * Проверяет, находится ли человек в определенной точке плоскости
     *
     * @param name    имя человека
     * @param x       координаты x
     * @param y       координаты y
     * @param surface поверхность
     * @return true - если находится
     */
    public static boolean isOnXY(String name, int x, int y, Map<Point, String> surface) {
        // создаю объект "точка"
        Point point = new Point(x, y); // точка - как новый объект, по equals он не равен точке, которая уже лежит
        // по этой точке получаю имя человека
        // get полагает, что у Point есть адекватный hashCode и адекватный equals
        String existedHuman = surface.get(point);
        // возвращаю true, если там человек вообще есть и при этом его имя совпадает с тем, которе мы послали на вход
        return existedHuman != null && existedHuman.equals(name);
    }

    public static void main(String[] args) {
        // поверхность - набор точек и ассоциированных с ними объектов
        Map<Point, String> surface = new MapHashImpl<>();
        // наши точки
        Point a = new Point(10, 10);
        Point b = new Point(15, 15);
        Point c = new Point(20, 20);
        Point d = new Point(25, 25);
        // под каждой точкой кладем какой-либо строковый объект
        surface.put(a, "Марсель");
        surface.put(b, "Разиль");
        surface.put(c, "Айрат");
        surface.put(d, "Максим");

        System.out.println(isOnXY("Айрат", 20, 20, surface)); // true
        System.out.println(isOnXY("Максим", 20, 20, surface)); // false
        System.out.println(isOnXY("Максим", 30, 30, surface)); // false
    }

}
