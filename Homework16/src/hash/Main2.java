package hash;

import collections.Map;
import collections.MapHashImpl;

public class Main2 {

        public static void main(String[] args) {
            Map<String, Integer> map = new MapHashImpl<>();

            map.put("Марсель", 27);
            map.put("Виктор", 25);
            map.put("Айрат", 23);
            map.put("Марсель", 30);

            System.out.println(map.get("Марсель"));
            System.out.println(map.get("Виктор"));
            System.out.println(map.get("Айрат"));
            
        }
    }
