package collections;

/**
 * Created by Зульфат on 02.01.2022.
 */
public interface Set<E> {

    void add(E element);
    boolean contains(E element);

}
