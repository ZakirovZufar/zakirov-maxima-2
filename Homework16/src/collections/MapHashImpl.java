package collections;


public class MapHashImpl<K, V> implements Map<K, V> {

    private static final int MAP_SIZE = 16;

    private Entry<K, V>[] entries;

    private static class Entry<K, V> implements Map.MapEntry<K, V> {
        int hash;
        K key;
        V value;
        Entry<K, V> next;

        Entry(K key, V value, int hash) {
            this.key = key;
            this.value = value;
            this.hash = hash;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }
    }

    private int count;

    public MapHashImpl() {
        this.entries = new Entry[MAP_SIZE];
        this.count = 0;
    }

    @Override
    public void put(K key, V value) {
        // p = tab[i = (n - 1) & hash]
        // взяли хеш-код у ключа
        int hash = key.hashCode();
        // посчитали индекс
        int index = hash & (MAP_SIZE - 1);
        // если под этим индексом уже что-то лежит
        if (entries[index] != null) {
            // положим просто в конец этого списка значение
            // берем первый элемент текущей ячейки
            Entry<K, V> current = entries[index];

            // проверяем первый узел
            if (current.hash == hash && current.key.equals(key)) {
                // если ключ уже был - заменяем значение этого ключа
                current.value = value;
                // останавливаем работу метода
                return;
            }
            // проверяем остальные
            while (current.next != null) {
                // на каждом шаге надо проверить, а не попали ли мы на тот же самый ключ, который уже был
                if (current.hash == hash && current.key.equals(key)) {
                    // если ключ уже был - заменяем значение этого ключа
                    current.value = value;
                    // останавливаем работу метода
                    return;
                }
                // идем до конца, пока не дойдем до элемента, который последний в этом списке
                current = current.next;
            }
            // как только дошли до последнего - добавляем после него новый элемент
            current.next = new Entry<>(key, value, hash);
            count++;
        } else {
            // в противном случае положим значение
            entries[index] = new Entry<>(key, value, hash);
            count++;
        }
    }

    @Override
    public V get(K key) {
        // взяли хеш-код у ключа
        int hash = key.hashCode();
        // посчитали индекс
        int index = hash & (MAP_SIZE - 1);
        // если под этим индексом уже что-то лежит
        if (entries[index] != null) {
            // берем первый элемент текущей ячейки
            Entry<K, V> current = entries[index];
            while (current != null) {
                // если нашли совпадающий ключ
                if (current.hash == hash && current.key.equals(key)) {
                    // возвращаем значение
                    return current.value;
                }
                // если текущий узел нас не устроил - идем дальше
                current = current.next;
            }
            // если до этого ни разу не нашли ничего
            return null;
        } else {
            // если под этим индексом ничего нет - то значит в принципе тут ничего нет
            return null;
        }
    }

    @Override
    public boolean containsKey(K key) {
        int hash = key.hashCode();
        int index = hash & (MAP_SIZE - 1);

        if (entries[index] != null) {
            Entry<K, V> current = entries[index];

            while (current != null) {
                // проверяем, а не лежит ли там этот ключ
                if (current.hash == hash && current.key.equals(key)) {
                    // если есть этот ключ - то возвращаем true
                    return true;
                }
                // если ключ не нашли в текущем списке - идем дальше
                current = current.next;
            }
        }
        // в любом противном случае
        return false;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keySet = new SetHashImpl<>();
        // пробегаем все списки (backet-ы)
        for (int i = 0; i < entries.length; i++) {
            // если бакет не пустой
            if (entries[i] != null) {
                // он из себя представляет связный список, поэтому его нужно пробежать
                Entry<K, V> current = entries[i];
                // пока не прошли весь текущий бакет
                while (current != null) {
                    // кидаем в результат текущий ключ
                    keySet.add(current.key);
                    // идем дальше
                    current = current.next;
                }
            }
        }
        // возвращаю множество ключей
        return keySet;
    }

    @Override
    public Set<Map.MapEntry<K, V>> entrySet() {
        Set<Map.MapEntry<K, V>> entrySet = new SetHashImpl<>();

        // пробегаем все списки (backet-ы)
        for (int i = 0; i < entries.length; i++) {
            // если бакет не пустой
            if (entries[i] != null) {
                // он из себя представляет связный список, поэтому его нужно пробежать
                Entry<K, V> current = entries[i];
                // пока не прошли весь текущий бакет
                while (current != null) {
                    // кидаем в результат текущую пару
                    entrySet.add(current);
                    // идем дальше
                    current = current.next;
                }
            }
        }
        // возвращаю множество ключей
        return entrySet;

    }
}
