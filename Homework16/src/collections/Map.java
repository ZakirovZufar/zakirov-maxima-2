package collections;

/**
 * Created by Зульфат on 26.12.2021.
 */
public interface Map<K, V> {

    // int a[5] = 6;
    // map.put(5, 6);
    void put(K key, V value);

    // int i = a[5]; // i = 6
    // int i = map.get(5); // i = 6
    V get(K key);

    boolean containsKey(K key);

    Set<K> keySet();

    interface MapEntry<K, V> {
        K getKey();
        V getValue();
    }

    Set<MapEntry<K, V>> entrySet();

}
