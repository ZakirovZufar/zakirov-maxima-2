package collections;

/**
 * Created by Зульфат on 02.01.2022.
 */
public class SetHashImpl<E> implements Set<E> {

    // пустой объект
    private static final Object PRESENT = new Object();

    private MapHashImpl<E, Object> hashMap;

    public SetHashImpl() {
        this.hashMap = new MapHashImpl<>();
    }

    @Override
    public void add(E element) {
        this.hashMap.put(element, PRESENT);
    }

    @Override
    public boolean contains(E element) {
        return hashMap.containsKey(element);
    }

}
