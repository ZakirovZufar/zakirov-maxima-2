package singleton;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Logger logger = Logger.getInstance();
        Logger logger1 = Logger.getInstance();

        logger.addWord("иван");
        logger.addWord("топор");
        logger.addWord("пила");
        logger.addWord("нож");
        logger.addWord("сумка");

        logger1.addWord("дерево");
        logger1.addWord("береза");
        logger1.addWord("дуб");
        logger1.addWord("сосна");
        logger1.addWord("ель");

        logger.log();

        Scanner scanner = new Scanner(System.in);

        while (true) {
            String word = scanner.nextLine();
            System.out.println(logger1.complete(word));
        }
    }
}
