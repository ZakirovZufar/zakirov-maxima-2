package singleton;


public class Logger {

    private final static Logger instance;

    static {
        instance = new Logger();
    }

    private Logger() {
        words = new String[MAX_WORDS_COUNT];
    }

    public static Logger getInstance() {
        return instance;
    }

    private final static int MAX_WORDS_COUNT = 15;

    private String words[];
    private int count;


    public void log() {
        String out = "Приветствую вас в словаре Logger";
        System.out.println(out);
    }

    public void addWord(String word) {
        this.words[count] = word;
        count++;
    }

    public String complete(String prefix) {
        for (int i = 0; i < count; i++) {
            if (words[i].startsWith(prefix)) {
                return words[i];
            }
        }
        return "";
    }
}
