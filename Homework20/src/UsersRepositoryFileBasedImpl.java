import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class UsersRepositoryFileBasedImpl {

    private String fileName;

    public UsersRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    // возвращает список всех пользователей из файла
    public List<User> findAll() {
        List listUsers = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            String readerUser;
            while ((readerUser = reader.readLine()) != null) {
                listUsers.add(readerUser);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }  return listUsers;
    }

    public Optional<User> findByFirstName(String firstName) {
       try(BufferedReader readerFile = new BufferedReader(new FileReader(fileName))) {
           String userLine = readerFile.readLine() ;
           while (userLine != null){
               String[] arrayUser = userLine.split("\\|");
               if (arrayUser[0].equals(firstName)){
                   String nameUser = arrayUser[0];
                   String firstNameUser = arrayUser[1];
                   User user = new User(nameUser,firstNameUser);
                   return Optional.of(user);
               }
               userLine = readerFile.readLine();
           }
           return Optional.empty();
       }catch (IOException e){
           throw new IllegalArgumentException(e);
       }
    }

    public void save(User user) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.write(user.getFirstName() + "|" + user.getLastName());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // выполняется всегда, независимо от ошибок
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
        }
    }
}
