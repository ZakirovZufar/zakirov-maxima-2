public class Main {

    public static void main(String[] args) {

        UsersRepositoryFileBasedImpl usersRepository = new UsersRepositoryFileBasedImpl("users.txt");
        User marsel = new User("Марсель", "Сидиков");
        User maxim = new User("Максим", "Анисимов");
        User ravil = new User("Равиль", "Фахрутдинов");
        User zufar = new User("Зуфар", "Закиров");
        User ivan = new User("Иван", "Ургант");

//        usersRepository.save(marsel);
//        usersRepository.save(maxim);
//        usersRepository.save(ravil);
//        usersRepository.save(zufar);
//        usersRepository.save(ivan);

        System.out.println(usersRepository.findAll());
        System.out.println(usersRepository.findByFirstName("Марсель"));


    }
}

