public class Main {

    public static void main(String[] args) {
        // реализовать систему платежей
        // один пользователь может переводить деньги другому пользователю

        User marsel = new User("+79372824941", "Марсель");
        User rafael = new User("+79372824942", "Рафаэль");


        Bank bank = new Bank();
        bank.sendMoney(rafael, marsel, 100);
        bank.sendMoney(rafael, marsel, 300);
        bank.sendMoney(rafael, marsel, 200);
        bank.sendMoney(marsel, rafael, 50);
        bank.sendMoney(marsel, rafael, 100);
        bank.sendMoney(marsel, rafael, 120);
        bank.sendMoney(marsel, rafael, 220);

        System.out.println(bank.getTransactionsSum(marsel));
        bank.allTransactionsUser();

    }

}

