import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Bank {
    private Map<User, List<Transaction>> transactions;

    public Bank() {
        this.transactions = new HashMap<>();
    }

    // метод для перевода денег
    public void sendMoney(User from, User to, int sum) {
        // создаем сам перевод
        Transaction transaction = new Transaction(from, to, sum);
        // проверить, если ли уже у нас история переводов этого пользователя или нет
        // если еще переводов не было
        if (!transactions.containsKey(from)) {
            // кладу пользователю пустой список
            transactions.put(from, new ArrayList<>());
        }
        // get - получает список всех транзакций пользователя from и добавляет транзакцию в этот список
        transactions.get(from).add(transaction);
    }

    public List<Transaction> getTransactionsByUser(User user) {
        return transactions.get(user);
    }

    // вернуть сумму всех транзакций по конкретному пользователю
    public int getTransactionsSum(User user) {
        //проверяем, есть ли такой пользователь
        if (!transactions.containsKey(user)) {
            System.out.println("Нет такого пользователя");
        }

        //создаем список, чтобы туда положить все транзакции пользователя
        List<Transaction> list = getTransactionsByUser(user);

        int totalSum = 0;
        //проходим по списку,чтобы вычислить сумму всех транзакций
        for (Transaction operation : list) {
            totalSum += operation.getSum();
        }
        return totalSum;
    }

    //Вывести все имена пользователей и количество их транзакций
    public void allTransactionsUser() {
        // проходимся по нашей Map-е, и достаем оттуда Key, где ключом является пользователь,
        // и Value, где значение  кол-во транзакций
        for (Map.Entry<User, List<Transaction>> map : transactions.entrySet()) {
            System.out.println(map.getKey().getName() + " - " + map.getValue().size());
        }
    }

}





