import java.util.StringJoiner;

public class Transaction {
    private User from;
    private User to;
    private int sum;

    public Transaction(User from, User to, int sum) {
        this.from = from;
        this.to = to;
        this.sum = sum;
    }

    public User getFrom() {
        return from;
    }

    public User getTo() {
        return to;
    }

    public int getSum() {
        return sum;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Transaction.class.getSimpleName() + "[", "]")
                .add("from=" + from)
                .add("to=" + to)
                .add("sum=" + sum)
                .toString();
    }

}
