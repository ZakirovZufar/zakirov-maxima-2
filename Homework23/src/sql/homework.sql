drop table food_products;

create table food_products(
  id serial primary key,
  product_name char(40),
  price_product DOUBLE PRECISION,
  expiration_date int,
  date_of_receipt DATE,
  provider CHAR(30)
);

INSERT INTO food_products (product_name,price_product,expiration_date,date_of_receipt,provider)
    VALUES ('Milk',50.0,'10','2022-02-20','dairy plant');
INSERT INTO food_products (product_name,price_product,expiration_date,date_of_receipt,provider)
    VALUES('Sausage',250.0,30,'2022-02-01','sausage plant');
INSERT INTO food_products (product_name,price_product,expiration_date,date_of_receipt,provider)
    VALUES('Cheese',150.0,14,'2022-02-10','dairy plant');
INSERT INTO food_products (product_name,price_product,expiration_date,date_of_receipt,provider)
    VALUES('Bacon',90.0,21,'2022-02-01','sausage plant');

-- Товары,которые дороже 100 рублей
SELECT product_name,price_product from food_products where price_product > 100;

-- Товары, которые были поставлены ранее 02-02-2020 года
SELECT product_name,date_of_receipt from food_products where date_of_receipt > '2022-02-02';

-- Получение всех поставщиков
SELECT DISTINCT provider from food_products;

