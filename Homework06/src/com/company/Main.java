package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] array = new int[size];
        int min, indexOfMin;
        boolean hasElement = false;

        for(int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }

        System.out.println(Arrays.toString(array));

        for (int i = 0; i < array.length; i++){
            min = array[i];
            indexOfMin = i;

            for (int j = i; j < array.length; j++){
                if(array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }

            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;

            System.out.println(Arrays.toString(array));
            System.out.println("");
        }

        int element = scanner.nextInt();
        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;

        while (left <= right){
            if (element < array[middle]) {
                right = middle - 1;
            } else if (element > array[middle]){
                left = middle + 1;
            } else {
                hasElement = true;
                break;
            }
            middle = left + (right - left)/2;
        }

        System.out.println(hasElement);
    }
}
