import java.io.*;


public class TextConverter {

    public void toLowerCaseAll(String sourceFileName, String targetFileName) {
        FileInputStream readFile = null;
        try {
            readFile = new FileInputStream(sourceFileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        char[] characters;

        try {
            characters = new char[readFile.available()];
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        int position = 0;
        try {
            int currentByte = readFile.read();

            while (currentByte != -1) {
                // преобразуем байт в символ
                char character = (char) currentByte;
                // кидаем символ в массив
                characters[position] = character;
                position++;
                // считываем байт заново
                currentByte = readFile.read();
            }
            for (int i = 0; i < characters.length; i++) {
                if (!Character.isLetter(characters[i])) {
                    characters[i] = ' ';
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        String text = new String(characters);
        String lowerRegister = text.toLowerCase();

        try {
            FileOutputStream outputStream = new FileOutputStream(targetFileName);
            byte[] bytes = lowerRegister.getBytes();
            outputStream.write(bytes);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }
}




