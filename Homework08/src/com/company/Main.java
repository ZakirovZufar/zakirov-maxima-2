package com.company;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        while (number != -1) {
            int digitsSum = getDigitSum(number);
            System.out.println(digitsSum);
            number = scanner.nextInt();
        }
    }

    public static int getDigitSum(int x) {
        int digitsSum = 0;
        while (x != 0) {
            System.out.println("->> f(" + x + ")");
            digitsSum += x % 10;
            x = x / 10;
            System.out.println("<<-- f() = " + digitsSum);
        }
        return digitsSum;
    }
}

