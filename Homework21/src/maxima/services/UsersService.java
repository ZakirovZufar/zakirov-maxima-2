package maxima.services;

import maxima.services.exceptions.AuthenticationException;

/**
 * Created by Зульфат on 08.02.2022.
 */
public interface UsersService {

    /**
     * Регистрирует пользователя в системе
     * @param email Email пользователя
     * @param password пароль пользователя
     */
    void signUp(String email, String password);

    /**
     * Проверяет, есть ли пользователь в базе данных с такими учетными данными
     * @param email Email-пользователя
     * @param password Пароль пользователя
     * @throws AuthenticationException в случае, если email/пароль неверные
     */
    void signIn(String email, String password) throws AuthenticationException;

}
