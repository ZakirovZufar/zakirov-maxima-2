package maxima.validators;

import maxima.validators.exceptions.PasswordValidationException;

/**
 * Created by Зульфат on 08.02.2022.
 */
public class PasswordLengthValidator implements PasswordValidator {
    @Override
    public void validate(String password) throws PasswordValidationException {
        if (password.length() < 5) {
            throw new PasswordValidationException();
        }
    }

}
