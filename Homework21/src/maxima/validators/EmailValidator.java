package maxima.validators;

import maxima.validators.exceptions.EmailValidationException;

/**
 * Created by Зульфат on 08.02.2022.
 */
public interface EmailValidator {
    void validate(String email) throws EmailValidationException;
}
