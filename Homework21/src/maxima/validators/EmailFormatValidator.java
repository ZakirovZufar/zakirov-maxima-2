package maxima.validators;

import maxima.validators.exceptions.EmailValidationException;

/**
 * Created by Зульфат on 08.02.2022.
 */
public class EmailFormatValidator implements EmailValidator {
    @Override
    public void validate(String email) throws EmailValidationException {
        if (!email.contains("@")) {
            throw new EmailValidationException();
        }
    }


}
