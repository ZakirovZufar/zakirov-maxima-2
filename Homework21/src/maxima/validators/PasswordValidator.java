package maxima.validators;

import maxima.validators.exceptions.PasswordValidationException;

/**
 * Created by Зульфат on 08.02.2022.
 */
public interface PasswordValidator {
    void validate(String password) throws PasswordValidationException;
}
