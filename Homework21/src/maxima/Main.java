package maxima;

import maxima.repositories.UsersRepository;
import maxima.repositories.UsersRepositoryFilesImpl;
import maxima.services.UsersServiceImpl;
import maxima.util.IdGenerator;
import maxima.util.IdGeneratorFilesImpl;
import maxima.validators.*;

public class Main {

    public static void main(String[] args) {
        //  UsersRepository usersRepository = new UsersRepositoryFakeImpl();
        IdGenerator idGenerator = new IdGeneratorFilesImpl("users_id_sequence.txt");
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt", idGenerator);
        EmailValidator emailValidator = new EmailFormatValidator();
        PasswordValidator passwordValidator = new PasswordLengthValidator();
        UsersServiceImpl usersService = new UsersServiceImpl(usersRepository, emailValidator, passwordValidator);
//        usersService.signIn("sidikov.marsel@gmail.com", "qwerty007");
//        usersService.signIn("user3@gmail.com", "qwerty009");
        usersService.signUp("user10@gmail.com","qwerty009");

    }
}

