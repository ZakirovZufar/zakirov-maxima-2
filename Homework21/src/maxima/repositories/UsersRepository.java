package maxima.repositories;

import maxima.models.User;

import java.util.Optional;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersRepository {
    void save(User user);

    Optional<User> findByEmail(String email);
}

