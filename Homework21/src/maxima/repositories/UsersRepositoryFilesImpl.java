package maxima.repositories;

import maxima.models.User;
import maxima.util.IdGenerator;

import java.io.*;
import java.util.Optional;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {
    // я не хочу, чтобы место хранения пользователей изменилось в процессе работы программы
    private final String fileName;

    private final IdGenerator generator;

    public UsersRepositoryFilesImpl(String fileName, IdGenerator generator) {
        this.fileName = fileName;
        this.generator = generator;
    }

    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            user.setId(generator.generate());
            writer.write(user.getId() + "|" + user.getEmail() + "|" + user.getPassword());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String userLine = reader.readLine();

            while (userLine != null) {
                String[] parsedUserLine = userLine.split("\\|");

                if (parsedUserLine[1].equals(email)) {
                    String idFromFile = parsedUserLine[0];
                    String emailFromFile = parsedUserLine[1];
                    String passwordFromFile = parsedUserLine[2];
                    User user = new User(Long.parseLong(idFromFile), emailFromFile, passwordFromFile);
                    return Optional.of(user);
                }
                // если if-не сработал, читаем новую строку
                userLine = reader.readLine();
            }
            // если дошли до конца файла и ничего не нашли, возвращаем пустой Optional
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
