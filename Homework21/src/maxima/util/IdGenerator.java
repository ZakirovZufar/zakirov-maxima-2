package maxima.util;

/**
 * Created by Зульфат on 08.02.2022.
 */
public interface IdGenerator {
    Long generate();
}
