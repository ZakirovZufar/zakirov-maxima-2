package models;
/**
 * Created by Зульфат on 26.02.2022.
 */
public class Products {
    private int id;
    private String productName;
    private double priceProduct;
    private int expirationDate;
    private String dataOfReceipt;
    private String provaiderName;



    public Products(int id, String productName, double priceProduct, int expirationDate, String dataOfReceipt, String provaiderName) {
        this.id = id;
        this.productName = productName;
        this.priceProduct = priceProduct;
        this.expirationDate = expirationDate;
        this.dataOfReceipt = dataOfReceipt;
        this.provaiderName = provaiderName;
    }

    public Products(String productName, double priceProduct, int expirationDate, String dataOfReceipt, String provaiderName) {
        this.productName = productName;
        this.priceProduct = priceProduct;
        this.expirationDate = expirationDate;
        this.dataOfReceipt = dataOfReceipt;
        this.provaiderName = provaiderName;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPriceProduct() {
        return priceProduct;
    }

    public void setPriceProduct(double priceProduct) {
        this.priceProduct = priceProduct;
    }

    public int getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(int expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getDataOfReceipt() {
        return dataOfReceipt;
    }

    public void setDataOfReceipt(String dataOfReceipt) {
        this.dataOfReceipt = dataOfReceipt;
    }

    public String getProvaiderName() {
        return provaiderName;
    }

    public void setProvaiderName(String provaiderName) {
        this.provaiderName = provaiderName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Products products = (Products) o;

        if (id != products.id) return false;
        if (Double.compare(products.priceProduct, priceProduct) != 0) return false;
        if (expirationDate != products.expirationDate) return false;
        if (productName != null ? !productName.equals(products.productName) : products.productName != null)
            return false;
        if (dataOfReceipt != null ? !dataOfReceipt.equals(products.dataOfReceipt) : products.dataOfReceipt != null)
            return false;
        return provaiderName != null ? provaiderName.equals(products.provaiderName) : products.provaiderName == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        temp = Double.doubleToLongBits(priceProduct);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + expirationDate;
        result = 31 * result + (dataOfReceipt != null ? dataOfReceipt.hashCode() : 0);
        result = 31 * result + (provaiderName != null ? provaiderName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Products{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", priceProduct=" + priceProduct +
                ", expirationDate=" + expirationDate +
                ", dataOfReceipt='" + dataOfReceipt + '\'' +
                ", provaiderName='" + provaiderName + '\'' +
                '}';
    }
}
