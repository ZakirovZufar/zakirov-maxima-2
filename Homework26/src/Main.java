import models.Products;
import repositories.ProductsRepository;
import util.factories.ProductsRepositoryFactory;

public class Main {

    public static void main(String[] args) {

        ProductsRepository productsRepository = ProductsRepositoryFactory.onJdbc();

        //метод для сохранения нового продукта
//        Products milk = new Products("Milk", 60, 10, "2022-02-15", "MILK");
//        productsRepository.save(milk);

        Products milk = productsRepository.findById(8).orElseThrow(IllegalArgumentException::new);
        milk.setPriceProduct(57);

        //метод для обновления продукта
        productsRepository.update(milk);
        //метод для удаления продукта
        productsRepository.delete(milk);

        //поиск продуктов по определенной цене
        System.out.println(productsRepository.findAllByPrice(50));
        //поиск продуктов по названию
        System.out.println(productsRepository.findOneByName("Bacon"));

    }
}
