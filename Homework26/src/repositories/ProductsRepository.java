package repositories;
import models.Products;
import java.util.List;
import java.util.Optional;


public interface ProductsRepository {

    List<Products> findAllByPrice(double price);

    Optional<Products> findOneByName(String name);

    void save(Products products);

    void update(Products products);

    Optional<Products> findById(int id);

    void delete(Products products);
}
