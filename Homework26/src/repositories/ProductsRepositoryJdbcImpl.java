package repositories;

import models.Products;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.sql.*;
import java.sql.PreparedStatement;

/**
 * Created by Зульфат on 26.02.2022.
 */
public class ProductsRepositoryJdbcImpl implements ProductsRepository {

    private final DataSource dataSource;

    public ProductsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //language=SQL
    private static final String SQL_SELECT_BY_PRICE = "SELECT * FROM food_products where price_product=?";

    //language=SQL
    private static final String SQL_SELECT_BY_PRODUCT_NAME = "SELECT *FROM food_products where product_name = ?";

    //language=SQL
    private static final String SQL_INSERT_PRODUCT = "INSERT INTO food_products(product_name,price_product,expiration_date," +
            "date_of_receipt,provider) VALUES (?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_UPDATE_PRODUCT = "UPDATE food_products SET product_name= ?, price_product = ?, " +
            "expiration_date = ?, date_of_receipt = ?,provider= ? where id = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "SELECT * FROM food_products WHERE id = ?";

    //language=SQL
    private static final String SQL_DELETE_PRODUCT = "DELETE from food_products where id = ?";

    @Override
    public List<Products> findAllByPrice(double price) {

        List arrayProducts = new ArrayList();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_PRICE)) {

            statement.setDouble(1, price);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Products products = new Products(
                            resultSet.getInt("id"),
                            resultSet.getString("product_name"),
                            resultSet.getDouble("price_product"),
                            resultSet.getInt("expiration_date"),
                            resultSet.getString("date_of_receipt"),
                            resultSet.getString("provider"));

                    arrayProducts.add(products);
                }
                return arrayProducts;
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Products> findOneByName(String name) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_PRODUCT_NAME)) {

            statement.setString(1, name);

            try (ResultSet resultSet = statement.executeQuery()) {

                if (resultSet.next()) {
                    Products products = new Products(
                            resultSet.getInt("id"),
                            resultSet.getString("product_name"),
                            resultSet.getDouble("price_product"),
                            resultSet.getInt("expiration_date"),
                            resultSet.getString("date_of_receipt"),
                            resultSet.getString("provider"));

                    return Optional.of(products);
                }
            }
            return Optional.empty();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Products products) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PRODUCT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, products.getProductName());
            statement.setDouble(2, products.getPriceProduct());
            statement.setInt(3, products.getExpirationDate());
            statement.setString(4, products.getDataOfReceipt());
            statement.setString(5, products.getProvaiderName());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert product");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            // если сгенерированных ключей нет
            if (!generatedKeys.next()) {
                throw new SQLException("Can't retrieve generated id");
            }

            int generatedId = generatedKeys.getInt("id");

            products.setId(generatedId);

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Products products) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PRODUCT)) {

            statement.setString(1, products.getProductName());
            statement.setDouble(2, products.getPriceProduct());
            statement.setInt(3, products.getExpirationDate());
            statement.setString(4, products.getDataOfReceipt());
            statement.setString(5, products.getProvaiderName());
            statement.setInt(6, products.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update product");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Optional<Products> findById(int id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {

            statement.setInt(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {

                if (resultSet.next()) {
                    Products products = new Products(
                            resultSet.getInt("id"),
                            resultSet.getString("product_name"),
                            resultSet.getDouble("price_product"),
                            resultSet.getInt("expiration_date"),
                            resultSet.getString("date_of_receipt"),
                            resultSet.getString("provider"));


                    return Optional.of(products);
                }
            }
            return Optional.empty();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Products products) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_PRODUCT)) {
            statement.setInt(1, products.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) throw new SQLException("Can't delete product");

        } catch (SQLException e) {

            throw new IllegalArgumentException(e);
        }
    }
}

