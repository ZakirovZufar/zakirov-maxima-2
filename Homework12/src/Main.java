public class Main {

    public static void main(String[] args) {
        Ellips ellips = new Ellips(10, 6, 5, 3);
        Rectangle rectangle = new Rectangle(8, 4, 4, 2);
        Circle circle = new Circle(6, 12, 3, 6);


        Figure [] figures = {ellips, rectangle, circle};

        for(int i = 0; i < figures.length; i++ ){
            figures[i].move(66,77);
        }

        System.out.println("Периметр эллипса - " + ellips.getPerimetr());
        System.out.println("Периметр прямоугольника - " + rectangle.getPerimetr());
        System.out.println("Периметр круга - " + circle.getPerimetr());

        System.out.println("Новая координата эллипса - " + ellips.x + " ; " + ellips.y );
        System.out.println("Новая координата прямоугольника - " + rectangle.x + " ; " + rectangle.y);
        System.out.println("Новая координата круга - " + circle.x + " ; " + circle.y);

    }
}
