
public class Circle extends Ellips {
    public Circle (int a, int b, int x, int y){
        super (a, b, x, y);
    }
    public double getPerimetr (){
        int radius = a;
        double perimetr = 2 * Math.PI * radius;
        return perimetr;
    }
}
