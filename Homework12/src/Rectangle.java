
public class Rectangle extends Figure {
    private int a;
    private int b;

    public Rectangle (int a, int b, int x, int y){
        super (x, y);
        this.a = a;
        this.b = b;
    }

    public double getPerimetr (){
        double perimetr = 2 * (a + b);
        return perimetr;
    }

}
