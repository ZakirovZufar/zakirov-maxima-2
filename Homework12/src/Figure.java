
public class Figure {
    protected int x;
    protected int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double getPerimetr(){
        return -1;
    }

    public void move(int newX, int newY){
        this.x = newX;
        this.y = newY;
    }


}
