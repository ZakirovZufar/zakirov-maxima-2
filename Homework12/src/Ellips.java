
public class Ellips extends Figure {
    protected int a;
    protected int b;


    public Ellips(int a, int b, int x, int y) {
        super(x, y);
        this.a = a;
        this.b = b;

    }

    public double getPerimetr (){
        double perimetr = 4 * (((Math.PI * a * b) + (a - b)) / (a + b));
        return perimetr;
    }
}