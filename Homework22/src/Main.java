import models.Car;
import repositories.CarRepository;
import repositories.CarRepositoryImpl;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        CarRepository carRepository = new CarRepositoryImpl("input.txt");
        System.out.println(carRepository.findAll());

        List<Car> byColorOrMileage = carRepository.findByColorOrMileage("Black", 0);
        // выводит кол-во авто в диапазоне от 700к до 800к
        List<Car> byPrice = carRepository.findByPrice(700000.0, 800000.0);
        //выводит цвет авто с минимальной стоимостью
        carRepository.findByMinPrice();
        //выводит среднюю цену авто
        carRepository.averagePrice();


        byColorOrMileage
                .stream()
                .map(Car::getNumber)
                .forEach(System.out::println);
        System.out.println("-------------------");

        byPrice
                .stream()
                .map(Car::getNumber)
                .forEach(System.out::println);
        System.out.println
                ("Количество машин в диапазоне от 700к и до 800к " + " - "
                        + byPrice.stream().count());
    }
}

