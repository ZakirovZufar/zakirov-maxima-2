package repositories;

import models.Car;

import java.util.List;

/**
 * Created by Зульфат on 13.02.2022.
 */
public interface CarRepository {
    List<Car> findAll();

    List<Car> findByColorOrMileage(String color, Integer mileage);

    List<Car> findByPrice(Double priceMin, Double priceMax);

    void findByMinPrice();

    void averagePrice();


}
