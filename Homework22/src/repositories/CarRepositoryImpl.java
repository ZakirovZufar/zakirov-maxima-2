package repositories;

import models.Car;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.codehaus.groovy.runtime.DefaultGroovyMethods.collect;

/**
 * Created by Зульфат on 13.02.2022.
 */
public class CarRepositoryImpl implements CarRepository {

    private String fileName;

    public CarRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Car> toCarMapper = string -> {
        String[] parsedLine = string.split("\\|");
        return new Car(parsedLine[0], parsedLine[1], parsedLine[2],
                Integer.parseInt(parsedLine[3]), Double.parseDouble(parsedLine[4]));
    };

    @Override
    public List<Car> findAll() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Car> findByColorOrMileage(String color, Integer mileage) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .filter(car -> car.getColor()
                            .equals(color) || car.getMileage().equals(mileage))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Car> findByPrice(Double priceMin, Double priceMax) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .filter(car -> (car.getPrice() >= priceMin && car.getPrice() <= priceMax))
                    .distinct()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void findByMinPrice() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println("Цвет автомобиля с минимальной стоимостью - " + reader.lines()
                    .map(toCarMapper)
                    .min(Comparator.comparing(Car::getPrice)).get().getColor());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void averagePrice() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println("Средняя стоимость Camry - " + reader.lines().map(toCarMapper)
                    .mapToDouble(Car::getPrice).average());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
