package com.company;

import javax.xml.bind.SchemaOutputResolver;

public class Main {

    public static double f(double x) {
        return x*x*x;
    }

    // Метод Сипсона

    public static double integralBySimpson(double a, double b, int n) {
        double width = (b - a) / n;
        double sum = 0;
        double sum2 = 0;

        for (double i = 1; i < n; i += 2) {
            sum += f (a + i * width);
            sum2 += f (a + (i + 1) * width);
        }
        return width/3 * (f (a) + 4 * sum + 2 * sum2);
    }

    public static void main(String[] args) {
        int[] ns = {10, 100, 1000, 10_000, 50_000, 100_000,
                150_000, 200_000, 300_000, 500_000, 1_000_000, 2_000_000};
        double from = 0;
        double to = 10;

        double[] ys = new double[ns.length];
        for (int i = 0; i < ns.length; i++) {
            ys[i] = integralBySimpson(from, to, ns[i]);
        }
        for (int i = 0; i < ns.length; i++ ){
            System.out.printf("|N = %5d| Y = %10.4f|\n", ns[i], ys[i]);

        }

    }
}


