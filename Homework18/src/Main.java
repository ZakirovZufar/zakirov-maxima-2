import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class Main {

    public static void main(String[] args) {

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("input.txt");
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        char[] characters;

        try {
            characters = new char[inputStream.available()];
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        int position = 0;
        try {
            int currentByte = inputStream.read();

            while (currentByte != -1) {
                // преобразуем байт в символ
                char character = (char) currentByte;
                // кидаем символ в массив
                characters[position] = character;
                position++;
                // считываем байт заново
                currentByte = inputStream.read();

            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        // создаем строку на основе массива
        String text = new String(characters);
        System.out.println(text);

        //Создаем коллекцию
        Map<String, Integer> count = new HashMap<>();

        //Делим строку на слова, и их кидаем в массив
        String[] words = text.split(" ");

        //Пробегаемся по массиву, считаем, сколько раз встречалось каждое слово
        for (String str : words) {
            if (count.containsKey(str)) {
                int lastCount = count.get(str);
                lastCount++;
                count.put(str, lastCount);
            } else {
                count.put(str, 1);
            }
            System.out.println(count);
        }
    }
}
