package com.company;

public class Bus {
    private int number;
    private String model;
    private Driver driver;

    // массив пассажиров
    private Passenger[] passengers;
    // фактическое количество пассажиров на данный момент
    private int count;

    public  Bus(int number, String model, int placesCount) {
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.model = model;
        // создали placesCount объектных переменных в которые можно положить пассажира
        this.passengers = new Passenger[placesCount];
    }

    public void incomePassenger(Passenger passenger) {
        // проверяем, не превысили ли мы количество мест?
        if (this.count < this.passengers.length) {
            this.passengers[count] = passenger;
            this.count++;
        } else {
            System.err.println("Автобус переполнен!");
        }
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
        driver.setBus(this);
    }

    public boolean isFull() {
        return this.count == passengers.length;
    }


}
