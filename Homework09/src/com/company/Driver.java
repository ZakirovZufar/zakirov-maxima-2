package com.company;


public class Driver {
    private String name1;
    private int experience;
    private Bus bus;
    private Passenger passenger;


    public Driver(String name, int experience) {
        this.name1 = name;
        if (experience > 0) {
            this.experience = experience;
        } else {

            System.err.println("ОПЫТА у " + name + "НЕТ!");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }



    // Метод, чтобы автобус поехал
    // Автобус поедет только тогда, когда не будет свободных мест

    public boolean drive(Bus bus) {
        if (bus.isFull()) {
            System.out.println("Автобус, водитель которого, " + name1 + " ,поехал!!!");
            return true;
        } else {
            System.err.println("В автобусе есть еще свободные места!!!");
            return false;
        }

    }
}







