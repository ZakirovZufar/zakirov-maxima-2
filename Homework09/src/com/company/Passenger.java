package com.company;

public class Passenger {
    private String name;

    // объектная переменная, поле, которое ссылается на какой-то автобус
    private Bus bus;
    private Driver driver;

    // перегруженные конструкторы
    public Passenger() {
        this.name = "Без имени";
    }

    public Passenger(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void goToBus(Bus bus) {
        // проверяем, а не в автобусе ли мы уже?
        if (this.bus != null) {
            System.err.println("Я, " + name + ", уже в автобусе!");
        } else {
            if (!bus.isFull()) {
                // если автобуса еще не было
                this.bus = bus;
                // передаем в автобус себя
                this.bus.incomePassenger(this);
            } else {
                System.err.println("Я, " + name + ", не попал в автобус(");
            }
        }
    }


    public void leaveBus(Bus bus) {

    }

}






